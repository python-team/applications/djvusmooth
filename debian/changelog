djvusmooth (0.3-2) UNRELEASED; urgency=medium

  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Fri, 18 Oct 2019 16:06:06 +0200

djvusmooth (0.3-1) unstable; urgency=medium

  * New upstream release.
  * deb/control:
    + add python-subprocess32 to build and runtime deps.
    + bump standards version to 4.4.0 (no changes needed).
  * deb/copyright:
    + expand copyright spans.
  * deb/tests/control:
    + add python-subprocess32 to test deps.
  * Update debian/docs.
  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

 -- Daniel Stender <stender@debian.org>  Tue, 10 Sep 2019 09:15:46 +0200

djvusmooth (0.2.19-3) unstable; urgency=medium

  * deb/control:
    + add !nocheck build profile switch to python-nose.
    + bump standards to 4.2.1 (no further changes needed).
    + remove ancient X-Python-Version field [Ondřej Nový].
  * deb/rules:
    + build with DH_VERBOSE=1 (according to standards 4.2.0).
    + drop unnecessary export line for DEB_BUILD_OPTIONS.
  * build with debhelper 11 (changes in deb/compat and deb/control).

 -- Daniel Stender <stender@debian.org>  Thu, 01 Nov 2018 17:20:58 +0100

djvusmooth (0.2.19-2) unstable; urgency=medium

  * deb/control: bump standards to 4.1.3 (no changed needed).
  * deb/copyright: expand copyright span.
  * add deb/gbp.conf.

  [ svn-git-automigration ]
  * deb/control: update Vcs fields (moved to salsa).

 -- Daniel Stender <stender@debian.org>  Wed, 07 Mar 2018 18:28:19 +0100

djvusmooth (0.2.19-1) unstable; urgency=medium

  * No upstream release.
  * deb/watch: drop version and script.

 -- Daniel Stender <stender@debian.org>  Mon, 25 Sep 2017 23:45:10 +0200

djvusmooth (0.2.18-2) unstable; urgency=medium

  * bump debhelper level to 10 (changes in deb/compat and deb/control).
  * deb/copyright:
    + use https in Format field.
    + expand copyright span.
  * deb/watch:
    + watch github.com/jwilk/djvusmooth.
    + drop uversionmangle (not needed).
    + switch to format version 4, add version and script.
  * deb/tests/djvusmooth:
    + purge locale/ folder before testing.
  * bump standards version to 4.0.0 (no further changes needed).

 -- Daniel Stender <stender@debian.org>  Sat, 15 Jul 2017 11:10:24 +0200

djvusmooth (0.2.18-1) unstable; urgency=medium

  * New upstream release.
  * deb/copyright: updated Source field (project moved to Github).

 -- Daniel Stender <stender@debian.org>  Mon, 28 Nov 2016 23:39:32 +0100

djvusmooth (0.2.17-1) unstable; urgency=medium

  * New upstream release.
  * deb/control:
    + put team into Maintainer field (unconfirmed team uploads).
    + update uploader's email address.
    + bump standards to 3.9.8 (no changes needed).
    + use HTTPS for Vcs-Browser.
  * deb/copyright:
    + updated copyright spans and email address.
  * drop deb/menu (lintian: command-in-menu-file-and-desktop-file).
  * set up DEP-8 tests.

 -- Daniel Stender <stender@debian.org>  Mon, 25 Jul 2016 21:12:47 +0200

djvusmooth (0.2.16-1) unstable; urgency=medium

  * New upstream release (Closes: #799017).
  * deb/control:
    + added dh-python to build deps.
    + put python-djvu and python-wxgtk3.0 also into build-deps (needed by the
      tests).
    + dropped Depends: against python-xdg (Closes: #793364).
  * deb/copyright: updated.
  * deb/watch: watch pypi.debian.net.

 -- Daniel Stender <debian@danielstender.com>  Tue, 15 Sep 2015 15:34:11 +0200

djvusmooth (0.2.15-1) unstable; urgency=medium

  * New upstream release (Closes: #760546, #762627)
    + dropped wxpython3.0-transition.patch (applied upstream).
    + updated djvusmooth.desktop.patch.
  * deb/control: bumped standards to 3.9.6 (no changes needed).
  * deb/rules: added DEB_BUILD_OPTIONS line.

 -- Daniel Stender <debian@danielstender.com>  Tue, 07 Oct 2014 22:47:54 +0200

djvusmooth (0.2.14-3) unstable; urgency=low

  * wxpython3.0 transition (Closes: #758950):
    * d/control: bumped dep on python-wxgtk to 3.0.
    * added wxpython3.0-transition.patch.
  * Fixed upstream-signing-key.
  * Improved djvusmooth.desktop.patch.

 -- Daniel Stender <debian@danielstender.com>  Mon, 25 Aug 2014 09:50:25 +0200

djvusmooth (0.2.14-2) unstable; urgency=low

  * deb/control:
    + bumped standards to 3.9.5 (no changes needed).
    + changed maintainer email address.
  * deb/copyright:
    + changed maintainer email address.
    + expanded copyrights to 2014.
  * deb/watch: added pgpsigurlmangle (also added deb/upstream-signing-key.pgp).

 -- Daniel Stender <debian@danielstender.com>  Thu, 26 Jun 2014 00:19:40 +0200

djvusmooth (0.2.14-1) unstable; urgency=low

  [ Daniel Stender ]
  * New upstream release.
  * Bumped Debhelper level to 9 (deb/control and compat).
  * deb/control:
    + bumped standards to 3.9.4 (no changes needed).
    + bumped X-Python-Version to 2.6 (according to Depends).
    + removed unnecessary dep versions (of python-all, djvulibre-bin,
      python-djvu).
  * deb/copyright:
    + extended copyrights to 2013.
  * deb/docs:
    + added doc/credits.txt.
  * .desktop:
    + removed desktop file from debian/.
    + added patch djvusmooth.desktop.patch.
    + changed debian/install.
  * Locales are now shipped with the upstream tarball:
    + deb/rules: removed execution of update-i18n script
      and dh_auto_clean overrides.
    + deb/control: removed b-d on gettext.
    + removed deb/source/options.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

 -- Daniel Stender <daniel@danielstender.com>  Fri, 05 Jul 2013 16:28:46 +0200

djvusmooth (0.2.11-1) unstable; urgency=low

  * New upstream release.
  * Changes of debian/copyright:
    + Removed the extra blank lines.
    + Changed DEP5 Format.
    + Changed URI from project page to code repo.
    + Removed the comma in the Copyright lines.
  * Changes of debian/control:
    + Bumped Standards-Version to 3.9.2 (no changes needed).
    + Changed X-Python-Version.
    + Removed build dep on python-support, added version to python-all
      (both for the build with dh_python2).
    + Removed alternative dep on python-wxgtk2.6 (Closes: #647739).
  * Re-written debian/rules to build with dh_python2.
  * Changes of debian/djvusmooth.desktop:
    + Added icon djvusmooth.xpm (LP: #926820).
    + Changed Name to begin with a capital letter.
    + Added German translation for GenericName.
  * Debian/install: added copying of icon.
  * Added extend-diff-ignore for po/*.po to source/options.

 -- Daniel Stender <daniel@danielstender.com>  Sun, 19 Feb 2012 23:53:51 +0200

djvusmooth (0.2.10-1) unstable; urgency=low

  * New upstream release.
  * Change of debian/copyright.
    + Extended copyrights to 2011.
    + Extra blank lines after check of DEP-5 with libconfig-model-perl.

 -- Daniel Stender <daniel@danielstender.com>  Fri, 04 Mar 2011 09:25:11 +0100

djvusmooth (0.2.8-3) unstable; urgency=low

  * Team upload.
  * Upload to unstable.

 -- Jakub Wilk <jwilk@debian.org>  Thu, 17 Feb 2011 18:17:41 +0100

djvusmooth (0.2.8-2) experimental; urgency=low

  * New maintainer (closes: #598155).
  * Minor changes on description text in debian/control.
  * Updated debian/copyright to meet CANDIDATE DEP-5.

 -- Daniel Stender <daniel@danielstender.com>  Fri, 14 Jan 2011 13:11:37 +0100

djvusmooth (0.2.8-1) experimental; urgency=low

  * New upstream release.
    + Fix handling non-ASCII filenames (closes: #595002).
    + Fix editing arrow/line annotations (closes: #595012).
  * Bump standards version to 3.9.1 (no changes needed).

 -- Jakub Wilk <jwilk@debian.org>  Tue, 31 Aug 2010 15:41:39 +0200

djvusmooth (0.2.7-1) unstable; urgency=low

  * New upstream release.

 -- Jakub Wilk <jwilk@debian.org>  Sat, 26 Jun 2010 21:01:57 +0200

djvusmooth (0.2.6-1) unstable; urgency=low

  * New upstream release:
    + Fix crash after save (closes: #585896).
  * Run doctests at build time.
    + Build-depend on python-all, python-nose.

 -- Jakub Wilk <jwilk@debian.org>  Tue, 15 Jun 2010 22:21:27 +0200

djvusmooth (0.2.4-1) unstable; urgency=low

  * New upstream release.
  * Switch to source format 3.0 (quilt).

 -- Jakub Wilk <jwilk@debian.org>  Wed, 31 Mar 2010 19:04:34 +0200

djvusmooth (0.2.3-1) unstable; urgency=low

  * New upstream release:
    + Fix issues with the “Overprinted annotation properties” dialog
      (closes: #574362, #574338).
    + Document that very narrow hyperlink borders are not portable
      (closes: #574361).
  * Let dh_installdocs install the upstream changelog.
  * Bump standards version to 3.8.4 (no changes needed).
  * Update my e-mail addresses.
  * Update homepage URL.

 -- Jakub Wilk <jwilk@debian.org>  Wed, 17 Mar 2010 22:10:21 +0100

djvusmooth (0.2.2-1) unstable; urgency=low

  * New upstream relase:
    - Works with both wxPython 2.8 and 2.6; adjust dependencies accordingly.

 -- Jakub Wilk <ubanus@users.sf.net>  Fri, 22 Jan 2010 19:43:06 +0100

djvusmooth (0.2.1-1) unstable; urgency=low

  * New upstream release.
  * Update debian/copyright.
  * Add Debian menu entry.
  * Add freedesktop.org desktop entry.
  * Use the lowercase “djvusmooth” spelling in debian/control.

 -- Jakub Wilk <ubanus@users.sf.net>  Mon, 11 Jan 2010 18:49:49 +0100

djvusmooth (0.2.0-1) unstable; urgency=low

  * Initial release (closes: #550922).

 -- Jakub Wilk <ubanus@users.sf.net>  Sun, 22 Nov 2009 10:28:47 +0100
